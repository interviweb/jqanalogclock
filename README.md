# jQuery analog clock

Fully customizable analog clock on canvas.

Install: **npm i jqanalogclock**

## Example

See real code in *example.html*.

### HTML

```
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv='content-type' content='text/html; charset=utf-8' />
        <title>JQAnalogClock test</title>
    </head>
    <body>
        <canvas id='clock-canvas' width='100' height='100'></canvas>
        <script type='text/javascript' src='/jquery.min.js'></script>
        <script type='text/javascript' src='/jqanalogclock.min.js'></script>
        <!-- PLUGIN CALL, SEE BELOW -->
    </body>
</html>
```

### Call plugin

#### Easy

```
$(document).ready(function() {
    $('#clock-canvas').drawAnalogClock();
});
```

#### Use interval update

```
$(document).ready(function() {
    $('#clock-canvas').drawAnalogClock(null, null, 1000);
});
```

#### Customized

```
$(document).ready(function() {
    $('#clock-canvas').drawAnalogClock(
        {
            'canvasBorder': null, 'clockBorder': null, 'bgColor': null,
            'hourPoints': null, 'minPoints': null, 'numbersMargin': 40,
            'numbersFont': '10px monospace'
        },
        true, 1000
    );
});
```

## API

* **drawAnalogClockBackground(data)** - draw clock background
* **drawAnaloglock(data, drawBackground, milliseconds)** - draw clock hands

### Parameters

* **data** - see below
* **drawBackground** - boolean, if true - drawing the background before drawing clock hands
* **milliseconds** - int, if true - start timer for update clock hands (stop if element not visible)

### Data options

```
{
    'canvasBorder': ['#000', 1], //color, width, use null for no draw
    'clockBorder': ['#000', 1], //see canvasBorder
    'bgColor': '#fff', //use null for no draw
    'bgImage': [null, 0, 0], //image, bx, by
    'pointsPadding': 5, //radius - padding
    //stroke color, fill color, stroke width, radius
    //use null for no draw, use null color for skip stroke or fill
    'hourPoints': ['#000', '#000', 1, 3],
    'minPoints': ['#000', null, 1, 1], //see hourPoints
    'secWidth': 1, //use zero for no draw
    'minWidth': 3, //use zero for no draw
    'hourWidth': 5, //use zero for no draw
    'secLength': 50,
    'minLength': 45,
    'hourLength': 30,
    'secColor': '#000',
    'minColor': '#000',
    'hourColor': '#000',
    //line cap for second hand, min, hour
    'cap': ['round', 'round', 'round'],
    //line join for second hand, min, hour
    'join': ['round', 'round', 'round'],
    'center': ['#000', '#000', 1, 4], //see hourPoints
    'numbers': [false, true], //stroke, fill, use null for no draw
    'numbersMargin': 50, //x, y center + margin
    'numbersFont': '12px serif',
    'numbersStrokeColor': '#000',
    'numbersFillColor': '#000',
    'numbersOffsets': [5, 5], //x, y
    'numbersOffsetsDetail': {'x': {12: 5}} //use null for no offset
}
```

* **canvasBorder** - square border
* **clockBorder** - circle clock border
* **bgColor** - background color in circle
* **bgImage** - canvas background image, drawing on 0, 0
* **pointsPadding** - padding from circle
* **hourPoints** - big hour points
* **minPoints** - small minutes points
* **cap** - see (this)[https://www.w3schools.com/tags/canvas_linecap.asp]
* **join** - see (this)[https://www.w3schools.com/tags/canvas_linejoin.asp]
* **center** - center in clock circle
* **numbersMargin** - margin from center
* **numbersOffsets** - offset from calculate points on circle
* **numbersOffsetsDetail** - personal offset for numbers
