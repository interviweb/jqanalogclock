(function($) {
    const default_data = {
        'canvasBorder': ['#000', 1], //color, width, use null for no draw
        'clockBorder': ['#000', 1], //see canvasBorder
        'bgColor': '#fff', //use null for no draw
        'bgImage': [null, 0, 0], //image, bx, by
        'pointsPadding': 5, //radius - padding
        //stroke color, fill color, stroke width, radius
        //use null for no draw, use null color for skip stroke or fill
        'hourPoints': ['#000', '#000', 1, 3],
        'minPoints': ['#000', null, 1, 1], //see hourPoints
        'secWidth': 1, //use zero for no draw
        'minWidth': 3, //use zero for no draw
        'hourWidth': 5, //use zero for no draw
        'secLength': 50,
        'minLength': 45,
        'hourLength': 30,
        'secColor': '#000',
        'minColor': '#000',
        'hourColor': '#000',
        //line cap for second hand, min, hour
        'cap': ['round', 'round', 'round'],
        //line join for second hand, min, hour
        'join': ['round', 'round', 'round'],
        'center': ['#000', '#000', 1, 4], //see hourPoints
        'numbers': [false, true], //stroke, fill, use null for no draw
        'numbersMargin': 50, //x, y center + margin
        'numbersFont': '12px serif',
        'numbersStrokeColor': '#000',
        'numbersFillColor': '#000',
        'numbersOffsets': [5, 5], //x, y
        'numbersOffsetsDetail': {'x': {12: 5}} //use null for no offset
    };

    function drawBg(canvas, context, data) {
        let rad = canvas.width < canvas.height ? canvas.width / 2 : canvas.height / 2;
        let xCenter = canvas.width / 2;
        let yCenter = canvas.height / 2;
        context.clearRect(0, 0, canvas.width, canvas.height);
        if (data['bgColor']) {
            context.fillStyle = data['bgColor'];
            context.moveTo(xCenter, yCenter);
            context.arc(xCenter, yCenter, rad, 0, 2 * Math.PI, true);
            context.fill();
        }
        if (data['bgImage'][0]) {
            context.drawImage.apply(null, data['bgImage']);
        }
        if (data['canvasBorder']) {
            context.beginPath();
            context.strokeStyle = data['canvasBorder'][0];
            context.lineWidth = data['canvasBorder'][1];
            context.strokeRect(0, 0, canvas.width, canvas.height);
            context.stroke();
            context.closePath();
        }
        if (data['clockBorder']) {
            let r = rad - context.lineWidth;
            context.beginPath();
            context.strokeStyle = data['clockBorder'][0];
            context.lineWidth = data['clockBorder'][1];
            context.arc(xCenter, yCenter, r, 0, 2 * Math.PI, true);
            context.stroke();
            context.closePath();
        }
        context.moveTo(xCenter, yCenter);
        let radPoints = rad - data['pointsPadding'];
        if (data['hourPoints'] || data['minPoints']) {
            for (let i = 0; i < 60; i++) {
                context.beginPath();
                let r = 0;
                if (i % 5 === 0) {
                    if (data['hourPoints']) r = data['hourPoints'][3];
                } else {
                    if (data['minPoints']) r = data['minPoints'][3];
                }
                if (r === 0) continue;
                let x = xCenter + radPoints * Math.cos(-6 * i * (Math.PI / 180) + Math.PI / 2);
                let y = yCenter - radPoints * Math.sin(-6 * i *(Math.PI / 180) + Math.PI / 2);
                context.arc(x, y, r, 0, 2 * Math.PI, true);
                if (i % 5 === 0) {
                    context.lineWidth = data['hourPoints'][2];
                    if (data['hourPoints'][0]) {
                        context.strokeStyle = data['hourPoints'][0];
                        context.stroke();
                    }
                    if (data['hourPoints'][1]) {
                        context.fillStyle = data['hourPoints'][1];
                        context.fill();
                    }
                } else {
                    context.lineWidth = data['minPoints'][2];
                    if (data['minPoints'][0]) {
                        context.strokeStyle = data['minPoints'][0];
                        context.stroke();
                    }
                    if (data['minPoints'][1]) {
                        context.fillStyle = data['minPoints'][1];
                        context.fill();
                    }
                }
                context.closePath();
            }
        }
        if (data['numbers']) {
            let nfd = data['numbersOffsetsDetail'];
            context.beginPath();
            context.font = data['numbersFont'];
            context.strokeStyle = data['numbersStrokeColor'];
            context.fillStyle = data['numbersFillColor'];
            for (let i = 1; i <= 12; i++) {
                //magic aligning
                let xoff = data['numbersOffsets'][0];
                let yoff = data['numbersOffsets'][1];
                if (nfd && nfd['x'] && nfd['x'][i]) {
                    xoff += nfd['x'][i];
                }
                if (nfd && nfd['y'] && nfd['y'][i]) {
                    xoff += nfd['y'][i];
                }
            	let x = xCenter - xoff + data['numbersMargin'] * Math.cos(-30 * i * (Math.PI / 180) + Math.PI / 2);
            	let y = yCenter + yoff - data['numbersMargin'] * Math.sin(-30 * i * (Math.PI / 180) + Math.PI / 2);
                if (data['numbers'][1]) context.fillText(i, x, y);
                if (data['numbers'][0]) context.strokeText(i, x, y);
            }
            context.closePath();
        }
    }

    function drawHands(canvas, data, cont, beforeBg) {
        let context = cont ? cont : canvas.getContext('2d');
        let rad = canvas.width < canvas.height ? canvas.width / 2 : canvas.height / 2;
        let xCenter = canvas.width / 2;
        let yCenter = canvas.height / 2;
        context.clearRect(0, 0, canvas.width, canvas.height);
        if (beforeBg) drawBg(canvas, context, data);
        let date = new Date();
        if (data['secWidth'] > 0) {
            let t = 6 * date.getSeconds();
            context.beginPath();
            context.strokeStyle = data['secColor'];
            context.lineWidth = data['secWidth'];
            context.lineCap = data['cap'][0];
            context.lineJoin = data['join'][0];
            context.moveTo(xCenter, yCenter);
            context.lineTo(
                xCenter + data['secLength'] * Math.cos(Math.PI/2 - t * (Math.PI / 180)),
                yCenter - data['secLength'] * Math.sin(Math.PI/2 - t * (Math.PI / 180))
            );
            context.stroke();
            context.closePath();
        }
        if (data['minWidth'] > 0) {
            let t = 6 * (date.getMinutes() + (1 / 60) * date.getSeconds());
            context.beginPath();
            context.strokeStyle = data['minColor'];
            context.lineWidth = data['minWidth'];
            context.lineCap = data['cap'][1];
            context.lineJoin = data['join'][1];
            context.moveTo(xCenter, yCenter);
            context.lineTo(
                xCenter + data['minLength'] * Math.cos(Math.PI/2 - t * (Math.PI / 180)),
                yCenter - data['minLength'] * Math.sin(Math.PI/2 - t * (Math.PI / 180))
            );
            context.stroke();
            context.closePath();
        }
        if (data['hourWidth'] > 0) {
            let t = 30 * (date.getHours() + (1 / 60) * date.getMinutes());
            context.beginPath();
            context.strokeStyle = data['hourColor'];
            context.lineWidth = data['hourWidth'];
            context.lineCap = data['cap'][2];
            context.lineJoin = data['join'][2];
            context.moveTo(xCenter, yCenter);
            context.lineTo(
                xCenter + data['hourLength'] * Math.cos(Math.PI/2 - t * (Math.PI / 180)),
                yCenter - data['hourLength'] * Math.sin(Math.PI/2 - t * (Math.PI / 180))
            );
            context.stroke();
            context.closePath();
        }
        if (data['center'][2] > 0) {
            context.beginPath();
            context.lineWidth = data['center'][2];
            context.moveTo(xCenter, yCenter);
            context.arc(xCenter, yCenter, data['center'][3], 0, 2 * Math.PI, true);
            if (data['center'][0]) {
                context.strokeStyle = data['center'][0];
                context.stroke();
            }
            if (data['center'][1]) {
                context.fillStyle = data['center'][1];
                context.fill();
            }
            context.closePath();
        }
        return context;
    }

    function getData(data) {
        if (data == null) return default_data;
        let result = {}
        for (key in default_data) {
            if (key in data) result[key] = data[key];
            else result[key] = default_data[key];
        }
        return result;
    }

    /**
    * Draw clock background (borders, color, image, points, numbers).
    *
    * @param {object} data Data map (see README.md)
    */
    $.fn.drawAnalogClockBackground = function(data) {
        this.each(function () {
            drawBg($(this)[0], $(this)[0].getContext('2d'), getData(data));
        });
    }

    /**
    * Draw clock hands (before draw background, optional).
    *
    * @param {object} data Data map (see README.md)
    * @param {boolean} drawBackground if true - draw background (see drawAnalogClockBackground)
    * @param {number} milliseconds update interval (if null - no start timer)
    */
    $.fn.drawAnalogClock = function(data, drawBackground, milliseconds) {
        this.each(function () {
            let d = getData(data);
            let e = $(this);
            let context = drawHands(e[0], d, null, drawBackground == null ? true : drawBackground);
            if (milliseconds) {
                function timer() {
                    setTimeout(function () {
                        if (e.is(':hidden')) return;
                        drawHands(e[0], d, null, drawBackground == null ? true : drawBackground);
                        timer();
                    }, milliseconds - (new Date()).getMilliseconds());
                }
                timer();
            }
        });
    }
}) (jQuery);
